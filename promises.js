const api = {};

const handler = (resolve, reject) => {
    return function(error, data) {
        if (error) {
            reject(error);
        } else {
            resolve(data);
        }
    }
};

const myApi = {
    createUser(data) {
        return new Promise((resolve, reject) => {
            api.createUser(data, handler(resolve, reject));
        });
    },
    getUserCurrent() {
        return new Promise((resolve, reject) => {
            api.getUserCurrent(handler(resolve, reject));
        });
    },
    getOrders() {
        return new Promise((resolve, reject) => {
            api.getOrders(handler(resolve, reject));
        });
    },
    getOrganizationInfo(id) {
        return new Promise((resolve, reject) => {
            api.getOrganizationInfo(id, handler(resolve, reject));
        });
    },
    pathOrganization(id) {
        return new Promise((resolve, reject) => {
            api.pathOrganization(id, handler(resolve, reject));
        });
    }
};

const save = (id, data) => {
    localStorage.set(id, JSON.stringify(data));
};

const scenario = () => {
    return myApi.getUserCurrent().then(userInfo => {
        if (userInfo) {
            save('userInfo', userInfo);

            const promises = [];

            if (userInfo.organization_id) {
                const getOrganizationInfoPromise = myApi.getOrganizationInfo(userInfo.organization_id)
                    .then(organizationInfo => {
                        save('organizationInfo', organizationInfo);
                    });

                promises.push(getOrganizationInfoPromise);
            }

            if (userInfo.scopes.indexOf('orders') !== -1) {
                const getOrdersPromise = myApi.getOrders().then(orders => {
                    save('orders', orders);
                });

                promises.push(getOrdersPromise);
            }

            return Promise.all(promises);
        } else {
            return myApi.createUser({
                first_name: 'Vasya',
                last_name: 'Petrov',
                scopes: []
            }).then(() => {
                return scenario();
            }).catch(() => console.log('Не в силах'));
        }
    });
};

scenario.then().catch();