;const drawNestedSetsTree = (function() {
    class NestedSetsTree {
        constructor(data, node) {
            this.data = data;
            this.node = node;

            this.dom = new Dom();

            this.state = this.nestedSetsToTree(this.sortNestedSets(this.data));

            this.render(this.state);
        }

        save() {
            return this.treeToNestedSets(this.state);
        }

        render(state) {
            this.dom.render(this.domTree(state), this.node);

            this.state = state;
        }

        domTree(state) {
            return {
                tag: 'ul',
                children: this.domTreeItems(state, state)
            };
        }

        domTreeItems(tree) {
            return tree.map(item => {
                const children = [item.title];

                if (item.children && item.children.length > 0) {
                    children.push({
                        tag: 'ul',
                        children: this.domTreeItems(item.children, this.state)
                    });
                }

                return {
                    tag: 'li',
                    children,
                    props: {
                        draggable: 'true',
                        dblclick: (e) => {
                            e.stopPropagation();

                            if (!this.isRoot(item.id)) {
                                const state = this.removeNodeWithoutChildren(this.state, item.id);
                                this.render(state);
                            }
                        },
                        dragstart: (e) => {
                            if (!this.isRoot(item.id)) {
                                e.stopPropagation();
                                e.dataTransfer.effectAllowed = 'move';
                                e.dataTransfer.setData('text', item.id);
                            } else {
                                e.preventDefault();
                            }
                        },
                        dragenter: (e) => {
                            e.preventDefault();
                            return true;
                        },
                        dragover: (e) => {
                            e.preventDefault();
                        },
                        drop: (e) => {
                            e.stopPropagation();

                            const id = Number(e.dataTransfer.getData('text'));
                            const node = this.findNode(this.state, id);
                            const child = this.findNode(node.children, item.id);

                            if (node && node.id !== item.id && !child) {
                                const state = this.moveNode(this.state, item.id, node);
                                this.render(state);
                            }
                        }
                    }
                };
            });
        }

        isRoot(id) {
            return !!this.state.find(item => item.id === id);
        }

        removeNodeWithoutChildren(state, id) {
            if (Array.isArray(state)) {
                const length = state.length;

                for (let i = 0; i < length; i++) {
                    const item = state[i];

                    if (item.id === id) {
                        const result = state.filter((item, key) => key !== i);

                        if (item.children && item.children.length > 0) {
                            return result.slice(0, i).concat(item.children).concat(result.slice(i));
                        }

                        return result;
                    } else if (Array.isArray(item.children)) {
                        const nodes = this.removeNodeWithoutChildren(item.children, id);

                        if (nodes !== item.children) {
                            state[i].children = nodes;
                        }
                    }
                }
            }

            return state;
        }

        removeNode(state, id) {
            return Array.isArray(state) ? state
                .filter(item => item.id !== id)
                .map(item => {
                    if (item.children) {
                        return Object.assign({}, item, {
                            children: this.removeNode(item.children, id)
                        });
                    }

                    return item;
                }) : state;
        }

        addNode(state, id, node) {
            return Array.isArray(state) ? state
                .map(item => {
                    if (item.id === id) {
                        const children = item.children ? [...item.children] : [];
                        const isSet = children.find(item => item.id === node.id);

                        if (!isSet) {
                            children.push(node);
                        }

                        return Object.assign({}, item, {children});
                    } else {
                        const nodes = this.addNode(item.children, id, node);

                        if (nodes !== item.children) {
                            return Object.assign({}, item, {children: nodes});
                        }
                    }

                    return item;
                }) : state;
        }

        moveNode(state, id, node) {
            return this.addNode(this.removeNode(state, node.id), id, node);
        }

        findNode(state, id) {
            if (Array.isArray(state)) {
                const length = state.length;

                for (let i = 0; i < length; i++) {
                    const item = state[i];

                    if (item.id === id) {
                        return item;
                    } else if (Array.isArray(item.children)) {
                        const node = this.findNode(item.children, id);

                        if (node) {
                            return node;
                        }
                    }
                }
            }

            return null;
        }

        formatItem(item, id) {
            return {
                id,
                title: item.title
            }
        }

        sortNestedSets(array) {
            return array.slice().sort((a, b) => {
                return Number(a.left) > Number(b.left) ? 1 : -1;
            });
        }

        nestedSetsToTree(array, parent = null, start = 0) {
            const length = array.length;
            const result = [];

            let prevItem = null;

            for (let i = start; i < length; i++) {
                const item = array[i];

                if (!parent) {
                    parent = item;
                }

                const isRoot = item.left === 1;

                const left = (item.left - parent.left) === 1;
                const right = (parent.right - item.right) === 1;
                const middle = prevItem && (item.left - prevItem.right) === 1;

                if (isRoot || left || middle || right) {
                    const node = this.formatItem(item, i);

                    if ((item.right - item.left) !== 1) {
                        const children = this.nestedSetsToTree(array, item, i + 1);

                        if (children.length > 0) {
                            node.children = children;
                        }
                    }

                    result.push(node);
                }

                if (left) {
                    prevItem = item;
                }

                if (right || item === parent) {
                    break;
                }
            }

            return result;
        }

        treeToNestedSets(tree, parent = null, index = 0, result = []) {
            let prev = index;

            tree.forEach((item, key) => {
                const nItem = {
                    title: item.title,
                    left: prev + 1
                };

                if (item.children && item.children.length > 0) {
                    result = this.treeToNestedSets(item.children, nItem, nItem.left, result);

                    prev = nItem.right;
                } else {
                    nItem.right = prev = nItem.left + 1;
                }

                if (tree.length === key + 1 && parent) {
                    parent.right = nItem.right + 1;
                }

                result.push(nItem);
            });

            return result;
        }
    }

    class Dom {
        constructor() {
            this.dom = null;
        }

        static createElement(node) {
            if (typeof node === 'string') {
                return document.createTextNode(node);
            }

            const element = document.createElement(node.tag);

            if (node.props) {
                for (let name in node.props) {
                    if (typeof node.props[name] === 'function') {
                        element['on' + name] = node.props[name];
                    } else {
                        element.setAttribute(name, node.props[name]);
                    }
                }
            }

            node.children.map(Dom.createElement).forEach((child) => {
                element.appendChild(child);
            });

            return element;
        }

        shouldReplace(newNode, oldNode) {
            return typeof newNode !== typeof oldNode ||
                typeof newNode === 'string' && newNode !== oldNode ||
                newNode.props !== oldNode.props ||
                newNode.tag !== oldNode.tag;
        }

        updateElement(domNode, newNode, oldNode, index = 0) {
            if (!oldNode) {
                domNode.appendChild(Dom.createElement(newNode));
            } else if (!newNode) {
                domNode.removeChild(domNode.childNodes[index]);
            } else if (this.shouldReplace(newNode, oldNode)) {
                domNode.replaceChild(Dom.createElement(newNode), domNode.childNodes[index]);
            } else if (newNode.children && oldNode.children) {
                const newLength = newNode.children.length;
                const oldLength = oldNode.children.length;
                for (let i = 0; i < newLength || i < oldLength; i++) {
                    this.updateElement(
                        domNode.childNodes[index],
                        newNode.children[i],
                        oldNode.children[i],
                        i
                    );
                }
            }
        }

        render(dom, node) {
            if (this.dom) {
                this.updateElement(node, dom, this.dom);
            } else {
                node.appendChild(Dom.createElement(dom));
                this.dom = dom;
            }
        }
    }

    return function(data, node) {
        return new NestedSetsTree(data, node);
    }
})();
