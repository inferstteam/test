function debounce(fn, ms) {
    let id = null;

    return function() {
        if (id) {
            clearTimeout(id);
        }

        id = setTimeout(fn.bind(null, ...arguments), ms);
    };
}

var foo = debounce(function(msg) {
    console.log(msg);
}, 5000);

foo('test 1');
setTimeout(() => foo('test 2'), 0);
setTimeout(() => foo('test 3'), 3000);
setTimeout(() => foo('test 4'), 7500);
setTimeout(() => foo('test 5'), 14000);
